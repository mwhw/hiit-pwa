# HIIT Timer

A progressive web app for HIIT on a spinning bike. No server needed, client only.

Demo (100x faster)

![Demo](hiit.gif)

## "Installation"

```bash
git clone https://gitlab.com/mwhw/hiit-pwa.git
cd hiit-pwa
python3 -m http.server --directory src/ 8000
```

iPad --> add to homescreen

## Setup

Change `timestep = 1000` in `modules/TimeClock.js` for use.

Training program in `modules/tabata.js`:

```javascript

var taba = [
    {
        name: "Workout 1",
        data: [
            {duration: 300, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            //...
            {duration: 300, intensity: 1, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }, {
        name: "Workout 2",
        data: [
            {duration: 300, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            //...
            {duration: 300, intensity: 2, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }
];
export {taba};
```

## TODO

* add music from spotify web-api depending on rpm of current exercise
