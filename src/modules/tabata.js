var taba = [
    {
        name: "Workout 1",
        data: [
            {duration: 60, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 60, intensity: 2, text: "Aufwärmen: Pedale ziehen", position: "Sitzen", rpm: 90},
            {duration: 60, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 60, intensity: 2, text: "Aufwärmen: Pedale ziehen", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 4, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 4, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 4, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 20, intensity: 5, text: "Level 3", position: "Stehen", rpm: 90},
            {duration: 20, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 20, intensity: 5, text: "Level 3", position: "Stehen", rpm: 90},
            {duration: 20, intensity: 1, text: "Erholung", position: "Sitzen", rpm: 90},
            {duration: 10, intensity: 6, text: "Level 4", position: "Stehen", rpm: 90},
            {duration: 300, intensity: 1, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }, {
        name: "Workout 2",
        data: [
            {duration: 300, intensity: 1, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 2, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 3, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 4, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 5, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 6, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 5, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 1, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 4, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 3, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 2, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 5, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 300, intensity: 2, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }, {
        name: "Workout 3",
        data: [
            {duration: 300, intensity: 2, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 2, text: "Level 3", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 2, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 2, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 30, intensity: 2, text: "Level 2", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 300, intensity: 2, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }, {
        name: "Workout 4",
        data: [
            {duration: 300, intensity: 2, text: "Aufwärmen", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 2, text: "Level 3", position: "Stehen", rpm: 90},
            {duration: 40, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 40, intensity: 2, text: "Level 1", position: "Stehen", rpm: 90},
            {duration: 30, intensity: 2, text: "Regeneration", position: "Sitzen", rpm: 90},
            {duration: 300, intensity: 2, text: "Cooldown", position: "Sitzen", rpm: 90},
        ]
    }
];
export {taba};