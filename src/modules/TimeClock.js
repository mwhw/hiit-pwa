// timerupdate alle <timestep> milliseconds; fürs debuggen niedriger als 1000 setzen;
let timestep = 10;
//let timestep = 1000;

function factory_showTime(element){
    var cb = function inner_cb() {
        var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59

        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;
        
        var time = h + ":" + m + ":" + s + " ";
        element.innerText = time;
        setTimeout(inner_cb, timestep);
    }
    cb();
}

var CentralCounter = function(){
    // wird vom Timer im Sekunden-Takt erhöht
    var i = 0;
    this.getTime = function() {return i; }
    this.inc = function() { return i++; }
}

var formatCounter = function(i) {
    var hours, minutes, seconds;
    hours = parseInt(i / 3600, 10);
    minutes = parseInt(i / 60, 10);
    seconds = parseInt(i % 60, 10);

    hours = hours == 0 ? "" : hours + ":";
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return hours + minutes + ":" + seconds;
}

var Timer = function() {
    var timerId;
    var myCounter = new CentralCounter();
    var callbackList = [];
    this.registerCallback = function(id, f) {
        callbackList[id] = {"f": f, "startTime": myCounter.getTime()};
    }
    this.unregisterCallback = function(id) {
        delete callbackList[id];
    }
    var callback = function() {
        //element.innerText = formatCounter(myCounter.getI());
        timerId = setTimeout(callback, timestep);
        let cumTime = myCounter.inc();

        for (let key in callbackList) {
            let f = callbackList[key]["f"];
            let startTime = callbackList[key]["startTime"];
            f(cumTime - startTime);
        }
    }

    this.pause = function() {
        window.clearTimeout(timerId);
    };

    this.resume = function() {
        window.clearTimeout(timerId);
        callback();
    };

    this.clear = function() {
        window.clearTimeout(timerId);
        callbackList = [];
    };
    this.reset = function() {
        window.clearTimeout(timerId);
        for (let key in callbackList) {
            let f = callbackList[key]["f"];
            f(0);
        }
    };

    this.start = this.resume;
};


// Exporting variables and functions
export { factory_showTime, Timer, formatCounter };