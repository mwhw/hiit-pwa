let chartWidth = document.querySelector("#graph").clientWidth - 20;
let chartHeight = document.querySelector("#graph").clientHeight - 20;

var makeChart = function(workout) {
    // Sample dataset. In a real application, you will probably get this data from another source such as AJAX.
    var dataset = [5, 10, 15, 20, 25]
    var dataset = workout.data;

    // Sizing variables for our chart. These are saved as variables as they will be used in calculations.
    /*var chartWidth = 700
    var chartHeight = 100*/
    var padding = 5

    // We want our our bars to take up the full height of the chart, so, we will apply a scaling factor to the height of every bar.
    var heightScalingFactor = chartHeight / getMax(dataset);

    let gesamtdauer = dataset.reduce( (a,b) => a+b["duration"] , 0);
    let nr = dataset.length;
    let widthScalingFactor = (chartWidth - (nr-1) * padding) / gesamtdauer;
    let xHelper = [], cum = 0;
    for (let d of dataset) {
        xHelper.push(cum);
        cum += d["duration"];
    }
    // https://en.wikipedia.org/wiki/Web_colors
    let colorHelper = ["DarkGreen", "Olive", "Gold", "Orange", "Crimson", "DarkRed"]

    document.querySelector("#graph").innerHTML = '';

    var svg = d3
    .select('#graph')
        .append('svg')
        .attr('width', chartWidth)
        .attr('height', chartHeight)

    // create the rectangles that will make up the bars in our bar chart
    svg
    .selectAll('rect')                                          // I'm selecting all of the rectangles in the SVG (note that at this point, there actually aren't any, but we'll be creating them in a couple of steps).
    .data(dataset)                                              // Then I'm mapping the dataset to those rectangles.
    .enter()                                                    // This step is important in that it allows us to dynamically create the rectangle elements that we selected previously.
        .append('rect')                                           // For each element in the dataset, append a new rectangle.
        .attr('x', function (value, index) {                    // Set the X position of the rectangle by taking the index of the current item we are creating, multiplying it by the calculated width of each bar, and adding a padding value so we can see some space between bars.
            return xHelper[index] * widthScalingFactor + padding * index;
            })
        .attr('y', function (value, index) {                    // Set the rectangle by subtracting the scaled height from the height of the chart (this has to be done becuase SVG coordinates start with 0,0 at their top left corner).
            return chartHeight - (value["intensity"] * heightScalingFactor)
        })
        .attr('width', (value, index) => value["duration"] * widthScalingFactor) // The width is dynamically calculated to have an even distribution of bars that take up the entire width of the chart.
        .attr('height', function (value, index) {               // The height is simply the value of the item in the dataset multiplied by the height scaling factor.
            return value["intensity"] * heightScalingFactor
        })
        .attr('fill', value => colorHelper[value["intensity"]-1])                                   // Sets the color of the bars.

    function getMax(collection) {
        return 6;
        /*var max = 0

        collection.forEach(function (element) {
            max = element > max ? element : max
        })

        return max*/
    }
}

var unhighlightRects = function() {
    let act = d3.select('#graph').selectAll("rect");
    act.attr("stroke-width", 0);
}
var highlightRect = function(nr) {
    let act = d3.select('#graph').select(`rect:nth-child(${nr})`);
    act.attr("stroke", "black");
    act.attr("stroke-width", 5);
}

// Exporting variables and functions
export { makeChart, unhighlightRects, highlightRect };