import * as TC from './modules/TimeClock.js';
import * as TG from './modules/TabataGraph.js';
import {taba} from './modules/tabata.js';

var select = document.querySelector("#actTabata");
for (let ind = 0; ind < taba.length; ind++) {
    var el = document.createElement("option");
    el.textContent = taba[ind]["name"];
    el.value = ind;
    select.add(el);
}

var populateWorkout = function() {
    let actInd = select.value;
    let workout = taba[actInd].data;
    let eleTaba = document.querySelector("#workout");
    eleTaba.innerHTML = '';
    eleTaba.insertAdjacentHTML('beforeend', `<div class="workoutrow">
            <div>Start</div>
            <div>Text</div>
            <div>Dauer</div> 
            <div>Sonstiges</div>
            <div>RPM</div>
        </div>`);
    var zeit = 0;
    for (var i = 0; i<workout.length; i++) {
        var item = workout[i];
        eleTaba.insertAdjacentHTML('beforeend', `<div class="workoutrow">
                <div class="clock">${TC.formatCounter(zeit)}</div>
                <div>${item["text"]}</div>
                <div class="clock animatedtimer">${TC.formatCounter(item["duration"])}</div>
                <div>${item["position"]}</div>
                <div>${item["rpm"]}</div>
            </div>`);
        zeit += item["duration"];
    }
    eleTaba.insertAdjacentHTML('beforeend', `<div class="workoutrow">
            <div class="clock">${TC.formatCounter(zeit)}</div>
            <div>ENDE</div> 
            <div></div> 
            <div></div>
            <div></div>
        </div>`);
    return zeit;
}
TC.factory_showTime(document.getElementById("Clock"));

var timer = new TC.Timer();

// die Callback-Funktion f(slot) soll nach Ihrer De-Registrierung f(slot + 1) registrieren
// außer es gibt keinen slot + 1 mehr in workout, d.h. slot >= workout.length
var factory_slotCallback = function(slot) {
    return function(i) {
        let actInd = select.value;
        let workout = taba[actInd].data;

        var duration = workout[slot]["duration"];
        let eleTaba = document.querySelector("#workout");
        let actrow = eleTaba.querySelectorAll(".workoutrow")[slot + 1]
        // Counter muss in die 3.Spalte
        document.querySelector("#Restzeit").innerText = TC.formatCounter( duration - i );
        document.querySelector("#Restzeit").style.width = Math.ceil(i/duration * 100) + "%";
        actrow.querySelector(":nth-child(3)").innerText = TC.formatCounter( duration - i );
        actrow.querySelector(":nth-child(3)").style.width = Math.ceil(i/duration * 100) + "%";

        if (i == 0) {
            //select actual row
            actrow.classList.add('actRow');
            TG.unhighlightRects();
            TG.highlightRect(slot + 1);
        }

        if (i == duration - 1){
            if (slot + 1 < workout.length) {
                timer.registerCallback("Timer" + (slot + 1), factory_slotCallback(slot + 1))

                let info = workout[slot]["rpm"] + "rpm (" + workout[slot]["position"] + ")";
                document.querySelector("#Info").innerText = info;
            } 
        }
        if (i > duration - 1) {
            timer.unregisterCallback("Timer" + slot);
            //unselect actual row
            actrow.classList.remove('actRow');
            actrow.classList.add('done');
            if (slot + 1 >= workout.length) {
                TG.unhighlightRects();
                // Workout beendet
                actrow.nextSibling.classList.add('actRow');
                timer.pause();
            }
        }
    }
}

var pauseResume = function() {
    let pr = document.querySelector("#PauseResume");
    if (pr.textContent == "Pause") {
        timer.pause();
        pr.textContent = "Resume";
    } else {
        timer.resume();
        pr.textContent = "Pause";
    }
}
document.querySelector("#PauseResume").onclick = pauseResume;


var resetStage = function() {
    let gesamtzeit = populateWorkout();
    timer.clear();
    timer.registerCallback( "Gesamtzeit", function(i) {
        document.querySelector("#Gesamtzeit").innerText = TC.formatCounter(i);
        document.querySelector("#Gesamtzeit").style.width = Math.ceil(i/gesamtzeit * 100) + "%";
    });
    timer.reset();
    let slot = 0;
    timer.registerCallback( "Timer" + slot, factory_slotCallback(slot));

    let actWorkout = document.querySelector("#actTabata").value;
    let info = taba[actWorkout].data[0]["rpm"] + "rpm (" + taba[actWorkout].data[0]["position"] + ")";
    document.querySelector("#Info").innerText = info;

    TG.makeChart(taba[actWorkout]);
    document.querySelector("#PauseResume").textContent = "Resume";
    document.querySelector("#Restzeit").style.width = "0%";
    document.querySelector("#Restzeit").innerText = TC.formatCounter(0);
};
document.querySelector("#Reset").onclick = resetStage;

select.addEventListener("change", function() {
    resetStage();
});

window.onload = function () {
    resetStage();
};


//throw new Error("MyError");